<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class aliexpressSearchController extends Controller
{
public static function index(Request $request){

	// Usage of path method
	$searchString = $request->input('keyword');
	//variables needed are searchString, minPrice, maxPrice
	$searchString= explode(" ", $searchString);
	$searchString=implode("+", $searchString);

	$vowels = array("\t", "\n", "\"", "  ");
	$rpl= array("", "", "'", "");
	$result_array= array();
	$page_iterator= 1;
	////loop freq determinant////
	//$url ='https://www.walmart.com/search/?page='.$page_iterator.'&query='.$searchString;
	$url = 'https://www.aliexpress.com/wholesale?SearchText='.$searchString.'&page=1';
	
	if(null !== ($minPrice= $request->input('minPrice'))){
		$url= $url.'&minPrice='.$minPrice;
		}
	if(null !== ($maxPrice= $request->input('maxPrice')))
			{
				$url= $url.'&maxPrice='.$maxPrice;
			}
	$conten = file_get_contents($url);
	//$conten = str_replace($vowels, $rpl, $conten);
	$result_space = explode( '<a  class="history-item product " href="//' , $conten );
	$space= count($result_space)-1;
	///////////////////
	if(null !== ($max_items= $request->input('max_items'))){
		$rows = $request->input('max_items');
	}
	else{
		$rows = 10;
	}
	$loop_over= round($rows / $space);
	if($loop_over == 0){
	$loop_over =1;
	}

	while($page_iterator <= $loop_over){

	
	$url = 'https://www.aliexpress.com/wholesale?SearchText='.$searchString.'&page='.$page_iterator;
	
	if(null !== ($minPrice= $request->input('minPrice'))){
		$url= $url.'&minPrice='.$minPrice;
		}
	if(null !== ($maxPrice= $request->input('maxPrice'))){
		$url= $url.'&maxPrice='.$maxPrice;
	}
	$conten = file_get_contents($url);
	//print_r($content);
	$result_space = explode( '<a  class="history-item product " href="//' , $conten );

	//
	//print_r($result_space);
	$iterator=1;
	//echo count($result_space);
	while($iterator < count($result_space)){
	$url_space = explode('" title="' , $result_space[$iterator] );
	$name_space5= $url_space[1];
	$url_space = str_replace($vowels, $rpl, $url_space[0]);

	//price
	//echo "ITEM PRICE<br>";
	$price_space = explode( '<span class="value" itemprop="price">US ' , $result_space[$iterator] );
	$price_space = explode('<strong class="free-s">Free Shipping</strong>' , $price_space[1] );
	$price_space_neural = str_replace($vowels, $rpl, $price_space[0]);
	$price_space_neural = explode('</span><span class=\'separator\'>/</span><span class=\'unit\'>' , $price_space_neural );
	$price_space= $price_space_neural[0].'/';
	$price_space_neural = explode('</span></span>' , $price_space_neural[1] );
	$price_space= $price_space.$price_space_neural[0];
	


	//item name
	//echo "ITEM NAME<br>";
	$name_space5 = explode( '" itemprop="name" target="_blank">' , $name_space5);
	$name_space5 = explode('" target="_blank">' , $name_space5[0] );
	$name_space5 = str_replace($vowels, $rpl, $name_space5[0]);


	
	//images url
	//echo "IMAGE URL<br>";
	$image_space = explode( 'class="picCore' , $result_space[$iterator-1] );
	$image_space = explode( 'src="' , $image_space[1] );
	$image_space = explode('"' , $image_space[1] );
	$image_space = str_replace($vowels, $rpl, $image_space[0]);
	//print_r($image_space[0]);
	//echo "<br><br>";

	$iterator++;

	$snip_array= array('name'=> $name_space5, 'image_url' => $image_space, 
	'price' => $price_space, 'details_url' => $url_space, 'website' => 'Aliexpress');

	array_push($result_array, $snip_array);
	}
	$page_iterator++;
}
	if( $rows < count($result_array) ){
		$result_array = array_splice($result_array,0 , $rows);
		return response()->json(['rows'=> count($result_array), 'content' => $result_array]);
		}
	else{
		return response()->json(['rows'=> count($result_array), 'content' => $result_array]);
	}
	}
}
