<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class ebaySearchController extends Controller
{
public static function index(Request $request){
		// Usage of path method
		$searchString = $request->input('keyword');
		//variables needed are searchString, minPrice, maxPrice
		$searchString= explode(" ", $searchString);
		$searchString=implode("+", $searchString);
	
		$vowels = array("\t","\r", "\n");
		$rpl= array("", "", "");
		$result_array= array();
		$page_iterator= 1;
		////loop freq determinant////
		$url ='https://www.ebay.com/sch/i.html?pgn='.$page_iterator.'&_nkw='.$searchString;
		if(null !== ($minPrice= $request->input('minPrice'))){
			$url= $url.'&_udlo='.$minPrice;
			}
		if(null !== ($maxPrice= $request->input('maxPrice')))
				{
					$url= $url.'&_udhi='.$maxPrice;
				}
		$conten = file_get_contents($url);
		$conten = str_replace($vowels, $rpl, $conten);
		$result_space = explode( '<div class="lvpicinner full-width picW"><a href="' , $conten );
		$space= count($result_space)-1;
		///////////////////
		if(null !== ($max_items= $request->input('max_items'))){
			$rows = $request->input('max_items');
			}
		else{
			$rows = 10;
		}
		$loop_over= round($rows / $space);
		if($loop_over == 0){
		$loop_over =1;
		}
	
		while($page_iterator <= $loop_over){
	$url ='https://www.ebay.com/sch/i.html?pgn='.$page_iterator.'&_nkw='.$searchString;
	
	if(null !== ($minPrice= $request->input('minPrice'))){
		$url= $url.'&_udlo='.$minPrice;
		}
	if(null !== ($maxPrice= $request->input('maxPrice')))
			{
				$url= $url.'&_udhi='.$maxPrice;
			}
	//&_udlo=20&_udhi=40
	$conten = file_get_contents($url);
	//print_r($content);
	//$result_space= array();
	$conten = str_replace($vowels, $rpl, $conten);
	$result_space = explode( '<div class="lvpicinner full-width picW"><a href="' , $conten );
	$iterator=1;
	//echo count($result_space);
	while($iterator < count($result_space)){
		//$url_space =  $result_space[$iterator];
		$url_space = explode('"  class="img imgWr2' , $result_space[$iterator] );
		$name_space5= $url_space[0];

		$name_space_neural = explode('\' /></a></div></div>' , $url_space[1]);
		$name_space_neural = explode(' alt=\'' , $name_space_neural[0]);
		$name_space = $name_space_neural[1];

		if (strpos($name_space_neural[1], '\' /></a>') !== false) {
			$name_space_neural = explode('\' /></a>' , $name_space_neural[1]);
			$name_space= $name_space_neural[0];
		}
		

		$price_space_neural = explode('lvprice prc"><span  class="bold">' , $url_space[1]);
		
		if (strpos($price_space_neural[1], '<span class="prRange">') !== false) {
			$price_space_neural = explode('<span class="prRange">' , $price_space_neural[1]);
			//$price_space = $price_space_neural[1];
			$price_space_neural = explode('<span>to</span>' , $price_space_neural[1]);
			$price_space= $price_space_neural[0];
			
			$price_space_neural = explode('</span>' , $price_space_neural[1]);
			$price_space= $price_space."-".$price_space_neural[0];
		}else{
			$price_space_neural = explode('</span>' , $price_space_neural[1]);
			$price_space= $price_space_neural[0];
		}
//imgurl=\"
		if (strpos($url_space[1], 'imgurl="') !== false) {
			$img_space_neural = explode('imgurl="' , $url_space[1]);
			//$price_space = $price_space_neural[1];
			$img_space_neural = explode('"onload' , $img_space_neural[1]);
			$img_space= $img_space_neural[0];
			
			
		}else{
			$img_space_neural = explode('<img  src="' , $url_space[1]);
			$img_space_neural = explode('" class="img"' , $img_space_neural[1]);
			$img_space = $img_space_neural[0];
		}		

		$iterator++;
        $snip_array= array( 'details_url' => $name_space5,'image_url' => $img_space, 
        'name' => $name_space, 'price' => $price_space, 'website' => 'Ebay');

		array_push($result_array, $snip_array);
	}
		$page_iterator++;
	}
	if( $rows < count($result_array) ){
		$result_array = array_splice($result_array,0 , $rows);
		return response()->json(['rows'=> count($result_array), 'content' => $result_array]);
		}
	else{
		return response()->json(['rows'=> count($result_array), 'content' => $result_array]);
	}
	
}
}
