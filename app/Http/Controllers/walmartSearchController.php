<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class walmartSearchController extends Controller
{
public static function index(Request $request){
	
	// Usage of path method
	$searchString = $request->input('keyword');
	//variables needed are searchString, minPrice, maxPrice
	$searchString= explode(" ", $searchString);
	$searchString=implode("+", $searchString);

	$vowels = array("\t","\r", "\n");
	$rpl= array("", "", "");
	$result_array= array();
	$page_iterator= 1;
	////loop freq determinant////
	$url ='https://www.walmart.com/search/?page='.$page_iterator.'&query='.$searchString;
	if(null !== ($minPrice= $request->input('minPrice'))){
		$url= $url.'&min_price='.$minPrice;
		}
	if(null !== ($maxPrice= $request->input('maxPrice')))
			{
				$url= $url.'&max_price='.$maxPrice;
			}
	$conten = file_get_contents($url);
	$conten = str_replace($vowels, $rpl, $conten);
	$result_space = explode( 'Product Image</span>' , $conten );
	$space= count($result_space)-1;
	///////////////////
	if(null !== ($max_items= $request->input('max_items'))){
		$rows = $request->input('max_items');
		}
	else{
		$rows = 10;
	}
	$loop_over= round($rows / $space);
	if($loop_over == 0){
	$loop_over =1;
	}

	while($page_iterator <= $loop_over){
	$url ='https://www.walmart.com/search/?page='.$page_iterator.'&query='.$searchString;

	if(null !== ($minPrice= $request->input('minPrice'))){
		$url= $url.'&min_price='.$minPrice;
		}
	if(null !== ($maxPrice= $request->input('maxPrice')))
			{
				$url= $url.'&max_price='.$maxPrice;
			}
	//&_udlo=20&_udhi=40
	$conten = file_get_contents($url);
	//print_r($content);
	//$result_space= array();
	$conten = str_replace($vowels, $rpl, $conten);
	$result_space = explode( 'Product Image</span>' , $conten );
	$price_space = '';
	$img_space ='';
	$iterator=1;
	//echo count($result_space);
	while($iterator < count($result_space)){
		//$url_space =  $result_space[$iterator];

		$url_space = explode('<a class="product-title-link" href="' , $result_space[$iterator] );
		$url_space = explode('" aria-label="' , $url_space[1]);
		$name_space5= $url_space[0];

		$img_space = explode('src="' , $result_space[$iterator] );
		$img_space = explode('"/></a>' , $img_space[1]);
		$img_space= $img_space[0];

		$name_space_neural = explode('"><span>' , $url_space[1]);
		$name_space= $name_space_neural[0];
		
		$price_space_neural = explode('price-main"><span class="Price-group" role="text" title="' , $result_space[$iterator]);
		$price_space_neural = explode('" aria-label' , $price_space_neural[1]);
		$price_space= $price_space_neural[0];
		
	

		$iterator++;
        $snip_array= array( 'details_url' => 'https://www.walmart.com'.$name_space5,'image_url' => 'https:'.$img_space, 
        'name' => $name_space, 'price' => $price_space, 'website' => 'Walmart');

		array_push($result_array, $snip_array);
		}
		$page_iterator++;
	}
	

	if( $rows < count($result_array) ){
		$result_array = array_splice($result_array,0 , $rows);
		return response()->json(['rows'=> count($result_array), 'content' => $result_array]);
		}
	else{
		return response()->json(['rows'=> count($result_array), 'content' => $result_array]);
	}
	
	}
}
