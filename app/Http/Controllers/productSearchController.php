<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\aliepxressSearchController;
use App\Http\Controllers\ebaySearchController;
use App\Http\Controllers\kongaSearchController;
use App\Http\Controllers\walmartSearchController;
use App\Http\Requests;

class productSearchController extends Controller
{
public function index(Request $request){

	if((null !== $request->input('platform')) and (null !== $request->input('keyword')) ){
		$platform = $request->input('platform');
		if($platform == 'aliexpress')
		{
			return aliexpressSearchController::index($request);
		}
		elseif($platform == 'ebay')
		{
			return ebaySearchController::index($request);
		}
		elseif($platform == 'walmart')
		{
			return walmartSearchController::index($request);
		}
		elseif($platform == 'konga')
		{
			return kongaSearchController::index($request);
		}	
	}
	else{
		return response()->json(['rows'=> 0, 'message' => 'missing parameters, check docs']);
	}
	
	
	}
}
