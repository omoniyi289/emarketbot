<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class kongaSearchController extends Controller
{
public static function index(Request $request){
	// Usage of path method
	$searchString = $request->input('keyword');
	//variables needed are searchString, minPrice, maxPrice
	$searchString= explode(" ", $searchString);
	$searchString=implode("+", $searchString);

	$vowels = array("\t", "\n", "\"", "  ");
	$rpl= array("", "", '"', "");
	$result_array= array();
	$price_space = '';
	$img_space ='';
	$name_space ='';
	$url = 'https://www.konga.com/catalogsearch/result/?q='.$searchString.'';
	
	if(null !== ($minPrice= $request->input('minPrice'))){
		$url= $url.'&price='.$minPrice;
		}
	if(null !== ($maxPrice= $request->input('maxPrice')))
			{
				$url= $url.'-'.$maxPrice;
			}
	//&_udlo=20&_udhi=40
	$conten = file_get_contents($url);
	//print_r($content);
	//$result_space= array();
	$conten = str_replace($vowels, $rpl, $conten);
	$result_space = explode( '<a class="product-block-link" href="' , $conten );
	$iterator=1;
	//echo count($result_space);
	while($iterator < count($result_space)){
	
		$url_space = explode('"><img class="catalog-product-image" src="' , $result_space[$iterator] );
		$name_space5= $url_space[0];
		//"
		$img_space = explode('<img class="catalog-product-image" src="' , $result_space[$iterator] );
		$img_space = explode('" /></a>' , $img_space[1] );
		$img_space= $img_space[0];

		$name_space_neural = explode('"><span>' , $url_space[1]);
		$name_space_neural = explode('</span></a>' , $name_space_neural[1]);
		$name_space= $name_space_neural[0];
		
		$price_space_neural = explode('&#8358;' , $url_space[1]);
		$price_space_neural = explode('</div>' , $price_space_neural[1]);
		$price_space= $price_space_neural[0];

		$iterator++;
        $snip_array= array( 'details_url' => 'https://www.konga.com'.$name_space5,'image_url' => $img_space, 
        'name' => $name_space, 'price' => 'NGN '.$price_space, 'website' => 'Konga');

		array_push($result_array, $snip_array);
	}
	


	return response()->json(['rows'=> count($result_space), 'content' => $result_array]);
	}
}
